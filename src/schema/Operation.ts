import Callback from "./Callback";
import ExternalDocumentation from "./ExternalDocumentation";
import Parameter from "./Parameter";
import Reference from "./Reference";
import RequestBody from "./RequestBody";
import SecurityRequirement from "./SecurityRequirement";
import Server from "./Server";

interface Operation {
  tags: Array<string>
  summary: string
  description: string
  externalDocs: ExternalDocumentation
  operationId: string
  parameters: Array<Parameter | Reference>
  requestBody: RequestBody | Reference
  responses: any
  callbacks: Map<string, Callback | Reference>
  deprecated: boolean
  security: SecurityRequirement
  servers: Array<Server>
}

export default Operation;
