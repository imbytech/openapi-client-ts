import Schema from './Schema';
import Response from './Response';
import Reference from './Reference';
import Parameter from './Parameter';
import Example from './Example';
import RequestBody from './RequestBody';
import Header from './Header';
import SecurityScheme from './SecurityScheme';
import Link from './Link';
import Callback from './Callback';
import PathItem from './PathItem';

interface Components {
  schemas: Map<string, Schema>
  responses: Map<string, Response | Reference>
  parameters: Map<string, Parameter | Reference>
  examples: Map<string, Example | Reference>
  requestBodies: Map<string, RequestBody | Reference>
  headers: Map<string, Header | Reference>
  securitySchemes: Map<string, SecurityScheme | Reference>
  links: Map<string, Link | Reference>
  callbacks: Map<string, Callback | Reference>
  pathItems: Map<string, PathItem | Reference>
}

export default Components;
