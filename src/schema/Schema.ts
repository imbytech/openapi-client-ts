import ExternalDocumentation from "./ExternalDocumentation";

interface Schema {
  // more fields than what is below, check the spec
  discriminator: any // check the Discriminator interface
  xml: any // check the XML interface
  externalDocs: ExternalDocumentation
  example: any
}

export default Schema;
