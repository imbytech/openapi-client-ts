import Operation from "./Operation";
import Parameter from "./Parameter";
import Reference from "./Reference";
import Server from './Server';

interface PathItem {
  $ref: string
  summary: string
  description: string
  get: Operation
  put: Operation
  post: Operation
  delete: Operation
  options: Operation
  head: Operation
  patch: Operation
  trace: Operation
  servers: Array<Server>
  parameters: Array<Parameter | Reference>
}

export default PathItem;
