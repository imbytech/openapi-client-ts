import ServerVariable from './ServerVariable';

interface Server {
  url: string
  description: string
  variables: Map<string, ServerVariable>
}

export default Server;
