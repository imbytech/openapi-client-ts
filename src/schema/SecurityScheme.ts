interface SecurityScheme {
  type: 'apiKey' | 'http' | 'mutualTLS' | 'oauth2' | 'openIdConnect'
  description: string
  // applies to type apiKey
  name: string
  // applies to type apiKey
  in: 'query' | 'header' | 'cookie'
  // applies to type http
  scheme: string
  // applies to type http ('bearer')
  bearerFormat: string
  // applies to type oauth2, check the OAuthFlows object in the spec
  flows: any
  // applies ot type openIdConnect
  openIdConnectUrl: string
}

export default SecurityScheme;
