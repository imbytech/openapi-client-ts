import Example from "./Example";
import MediaType from "./MediaType";
import Reference from "./Reference";
import Schema from "./Schema";

interface Parameter {
  name: string
  in: 'query' | 'header' | 'path' | 'cookie'
  description: string
  required: boolean
  deprecated: boolean
  allowEmptyValue: boolean
  // style attribute driving serialization
  // Default values (based on value of in):
  // for query - form; for path - simple; for header - simple; for cookie - form
  style: string
  // When style is form, the default value is true. For all other styles, the default value is false.
  explode: boolean
  allowReserved: boolean
  schema: Schema
  example: any
  examples: Map<string, Example | Reference>
  // for more complex scenarios
  content: Map<string, MediaType>
}

export default Parameter;
