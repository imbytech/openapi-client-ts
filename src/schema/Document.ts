import Info from './Info';
import Server from './Server';
//import Paths from './Paths';
import PathItem from './PathItem';
import Reference from './Reference';
import Components from './Components';
import SecurityRequirement from './SecurityRequirement';
import Tag from './Tag';
import ExternalDocumentation from './ExternalDocumentation';

interface Document {
  openapi: string
  info: Info
  jsonSchemaDialect: string
  servers: Array<Server>
  // not sure how to model the paths so leave it as a map for now
  paths: Map<string, PathItem>
  webhook: Map<string, PathItem | Reference>
  components: Components
  security: SecurityRequirement
  tags: Tag
  externalDocs: ExternalDocumentation
}

export default Document;
