import ParsingError from './ParsingError';
import RestRequest from './RestRequest';

/**
 * @module RestResponse
 */

/**
 * A simple REST response wrapper that parses the body on creation and
 * stores the resulting data.
 * @alias module:RestResponse
 * @class
 */
export default class RestResponse {
  request: RestRequest
  response: Response
  status: number
  body: any

  /**
   * Create a REST response given the original request that fetched the
   * response, the response itself and the parse body. This constructor
   * should not be used directly, the static fromRequestResponse function
   * should be used instead.
   * @param {RestRequest} request the REST request
   * @param {Response} response the HTTP response
   * @param {any} body the parsed body
   */
  constructor(request: RestRequest, response: Response, body: any) {
    this.request = request;
    this.response = response;
    this.status = response.status;
    this.body = body;
  }

  /**
   * Attempts to create a REST response given the original request and the
   * HTTP response by parsing the body according to the received content
   * type. The result is a promise that will resolve to a RestResponse or
   * reject to a ParsingError.
   * @param {RestRequest} request the original request
   * @param {Response} response the HTTP response
   */
  static fromRequestResponse(request: RestRequest, response: Response) {
    // TODO: add options to allow content specific handlers
    const { headers } = response;
    const contentType = (headers && headers.get('Content-Type')) || 'text/plain';
    let processor = null;
    if (contentType.startsWith('application/json')) {
      processor = (r: Response) => r.text().then(
        (text: string) => {
          let result = text;
          try {
            result = JSON.parse(text);
          } catch (e) {
            return Promise.reject(new ParsingError(
              `Could not parse body: ${e.message}`,
              request,
              response,
              e,
              contentType,
              text
            ));
          }
          return Promise.resolve(result);
        }
      );
    } else if (contentType.startsWith('text/')) {
      processor = (r: Response) => r.text();
    } else {
      processor = (r: Response) => r.blob();
    }
    return processor(response).then(
      body => Promise.resolve(new RestResponse(request, response, body)),
      error => Promise.reject(error)
    );
  }
}
