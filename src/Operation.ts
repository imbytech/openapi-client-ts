import RestRequest from './RestRequest';
import ResponseHandler from './ResponseHandler';

/**
 * @module Operation
 */

/**
 * An OpenApi operation.
 * @alias module:Operation
 * @class
 */
export default class Operation {
  baseUrl: string
  path: string
  method: string
  operationId: string
  parameters: Array<any>
  requestBody: any
  responses: object
  responseHandler: ResponseHandler

  /**
   * Create an operation based on configuration parameters.
   * @param {String} baseUrl the base URL for the service
   * @param {String} path the path for this operation relative to the base URL
   * @param {String} method the HTTP method for this operation
   * @param {any} op the operation's OpenApi configuration
   */
  constructor(baseUrl: string, path: string, method: string, op: any) {
    this.baseUrl = baseUrl;
    this.path = path;
    this.method = (method && method.toUpperCase()) || 'GET';
    const {
      operationId, parameters, requestBody, responses
    } = op;
    this.operationId = operationId;
    this.parameters = parameters || [];
    this.requestBody = requestBody || {};
    this.responses = responses || {};
    this.responseHandler = new ResponseHandler(this.responses);
  }

  /**
   * Check if the operation is read only based on its method.
   * This is typically used to identify whether the operation supports
   * query parameters or body parameters.
   * @returns {boolean} whether the operation is read only
   */
  isReadOnly() {
    return ['GET', 'HEAD', 'OPTIONS'].includes(this.method);
  }

  /**
   * Replace parameter placeholders in a path with the value of those
   * parameters. The path parameters need to be representable as string
   * that is suitable for inclusion in a path.
   * @param {string} path the path to update
   * @param {any} params the request parameters
   * @returns {string} the path with parameter placeholders replaced
   */
  resolvePathParameters(path: string, params: any) {
    const pathParams = this.parameters.filter(
      p => p.in === 'path'
    ).map(
      p => p.name
    );
    let resolvedPath = path;
    pathParams.forEach((p) => {
      const pathRegexp = new RegExp(`{${p}}`, 'g');
      const paramValue = params[p] || '';
      resolvedPath = resolvedPath.replace(pathRegexp, paramValue.toString());
    });
    return resolvedPath;
  }

  /**
   * Append all non-path parameters to the query if the method supports
   * query parameters.
   * @param {String} path the path to append query parameters to
   * @param {Object} params the request parameters
   * @returns {String} the path with query parameters appended or the
   *   original path if the operation doesn't support query parameters
   */
  resolveQueryParameters(path: string, params: any) {
    let result = path;
    if (this.isReadOnly() && params) {
      const pathParams = this.parameters.filter(
        p => p.in === 'path'
      ).map(
        p => p.name
      );
      const query = Object.entries(params).filter(
        entry => !(pathParams.includes(entry[0]))
      ).map(
        ([key, value]) => `${key}=${value}`
      ).join('&');
      if (query !== '') {
        result = [path, query].join('?');
      }
    }
    return result;
  }

  /**
   * Resolve the Content-Type header if it is not specified, according
   * to the following logic:
   * - if the body is set and is a Blob, use its content type
   * - if the OpenApi configuration has a requestBody block, use the first
   *   fully defined content type specified in that request
   * - otherwise default to application/json
   * @param {Object} options the request options
   * @returns {Object} updated request options
   */
  resolveContentType(options: any) {
    if (this.method !== 'GET' && this.method !== 'HEAD') {
      const headers = options.headers || {};
      let contentType = headers['Content-Type'] || null;
      if (contentType === null) {
        const { body } = options;
        // TODO: make this test more robust, e.g.:
        // https://stackoverflow.com/questions/20727750/how-to-check-if-a-variable-is-a-blob-in-javascript
        if (body && body.constructor.name === 'Blob' && body.type) {
          contentType = body.type;
        } else {
          const content = this.requestBody.content || {};
          const possibleContentTypes = Object.keys(content).filter(
            key => !key.includes('*')
          );
          if (possibleContentTypes.length > 0) {
            [contentType] = possibleContentTypes;
          }
        }
      }
      if (contentType === null) {
        contentType = 'application/json';
      }
      headers['Content-Type'] = contentType;
      return {
        ...options,
        headers
      };
    }
    return options;
  }

  /**
   * Resolve the Accept header if it is not specified by defaulting it
   * to application/json.
   * TODO: a better logic would be to check the content types specified
   * for the responses and use the first fully specified one.
   * @param {Object} options the request options
   * @returns {Object} updated request options
   */
  // eslint-disable-next-line class-methods-use-this
  resolveAccept(options: any) {
    const headers = options.headers || {};
    const Accept = headers.Accept || 'application/json';
    return {
      ...options,
      headers: {
        ...headers,
        Accept
      }
    };
  }

  /**
   * Create a JSON body for the request if that body doesn't exist and
   * if the operation supports body parameters.
   * TODO: make this a bit more robust and check that the content type
   * really is application/json before doing anything; future improvement:
   * add handlers for specific content types, in particular it would be
   * good to support application/x-www-form-urlencoded and
   * multipart/form-data.
   * See https://stackoverflow.com/questions/23714383/what-are-all-the-possible-values-for-http-content-type-header
   * for ideas of other content types to support.
   * @param {Object} options the request options
   * @param {Object} params the request parameters
   * @returns {Object} updated request options
   */
  resolveBodyParameters(options: any, params: any) {
    let result = options;
    const { body, headers } = options;
    const contentType = (headers && headers['Content-Type']) || '';
    if (!this.isReadOnly() && (body === null || body === undefined) && contentType.startsWith('application/json') && params) {
      const pathParams = this.parameters.filter(
        p => p.in === 'path'
      ).map(
        p => p.name
      );
      const nbody: any = {};
      Object.entries(params).filter(
        entry => !(pathParams.includes(entry[0]))
      ).forEach(
        (entry) => {
          const [key, value] = entry;
          nbody[key] = value;
        }
      );
      if (Object.keys(nbody).length > 0) {
        result = {
          ...options,
          body: JSON.stringify(nbody)
        };
      }
    }
    return result;
  }

  /**
   * Create a RestRequest object that can fetch data from the operation's
   * URL given a set of parameters and options.
   * @param {Object} params the request parameters
   * @param {Object} options the request options
   * @returns {RestRequest} the request that can fetch a response
   */
  request(params: any, options: any) {
    return new RestRequest(this.operationId, this.baseUrl, this.path, this.method, options)
      .mapPath(path => this.resolvePathParameters(path, params))
      .mapPath(path => this.resolveQueryParameters(path, params))
      .mapOptions(opts => this.resolveContentType(opts))
      .mapOptions(opts => this.resolveAccept(opts))
      .mapOptions(opts => this.resolveBodyParameters(opts, params));
  }

  thunk(requestThunk: any) {
    return this.responseHandler.thunk(requestThunk);
  }
}
