import ConnectionError from './ConnectionError';

/**
 * @module RestRequest
 */

/**
 * A simple REST request object. The aim of this class is to contain all
 * the information needed to fully configure a call to an API endpoint,
 * in such a form that can be re-tried, serialized and de-serialized as
 * necessary.
 * @alias module:RestRequest
 * @class
 */
export default class RestRequest {
  operationId: string
  url: string
  path: string
  method: string
  options: object

  /**
   * Create a REST request object given a set of parameters.
   * @param {String} operationId the ID of the API operation
   * @param {String} url the base URL of the API service
   * @param {String} path the path for this request, relative to the base URL
   * @param {String} method the HTTP method for this request
   * @param {Object} options request options such as headers, body, etc
   */
  constructor(operationId: string, url: string, path: string = '', method: string = 'GET', options: object = {}) {
    this.operationId = operationId;
    this.url = url;
    this.path = path;
    this.method = method;
    this.options = options;
  }

  /**
   * Return a thunk that can fetch a REST response from a REST request.
   * @returns {Function} a thunk that takes a RestRequest as argument
   *     and returns a Promise.
   */
  static thunk() {
    return (request: RestRequest) => {
      const fetchUrl = request.fetchUrl();
      const fetchOptions = {
        ...request.options,
        method: request.method
      };
      // TODO: that's the bit that needs to conditionally require fetch
      return fetch(fetchUrl, fetchOptions).catch(
        e => Promise.reject(new ConnectionError(
          `Connection error: ${e.message}`,
          request,
          e
        ))
      );
    };
  }

  /**
   * Map this request to another request by updating the path.
   * @param {Function} mapper the mapping function for the path
   * @returns {RestRequest} a new REST request with updated path
   */
  mapPath(mapper: (p: string) => string): RestRequest {
    return new RestRequest(
      this.operationId,
      this.url,
      mapper(this.path),
      this.method,
      this.options
    );
  }

  /**
   * Map this request to another request by updating the options.
   * @param {Function} mapper the mapping function for the options
   * @returns {RestRequest} a new REST request with updated options
   */
  mapOptions(mapper: (o: object) => object): RestRequest {
    return new RestRequest(
      this.operationId,
      this.url,
      this.path,
      this.method,
      mapper(this.options)
    );
  }

  /**
   * Create the fully qualified URL for this request.
   * @returns {String} the fully qualified URL
   */
  fetchUrl(): string {
    let baseUrl = this.url.toString();
    if (baseUrl.endsWith('/')) {
      baseUrl = baseUrl.slice(0, -1);
    }
    let basePath = this.path.toString();
    if (basePath.startsWith('/')) {
      basePath = basePath.slice(1);
    }
    return basePath === '' ? baseUrl : [baseUrl, basePath].join('/');
  }
}
