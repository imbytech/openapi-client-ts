import Operation from './Operation';
import RestRequest from './RestRequest';

/**
 * @module OpenApiClient
 */

/**
 * Provides an API client based on an OpenAPI document.
 * @alias module:OpenApiClient
 * @class
 */
export default class OpenApiClient {
  server: any
  url: string
  info: any
  operations: Array<Operation>
  securityConfig: any
  security: any
  securitySchemes: any
  securityHandler: any

  /**
   * Create an API client given an OpenAPI document.
   * @param {Object} config The OpenAPI document.
   * @param {Object} securityOptions Additional options for security such as:
   * <dl>
   * <dt>clientId</dt>
   * <dd>The Auth0 client ID</dt>
   * <dt>tokenStore</dt>
   * <dd>The async storage key for Auth tokens</dd>
   * <dt>navigate</dt>
   * <dd>Function that navigates back to the current screen</dd>
   * </dl>
   */
  constructor(config: any, securityOptions: any) {
    this.server = {};
    if (config.servers && config.servers.length >= 0) {
      this.server = config.servers[0] || {};
    }
    const { url } = this.server;
    const baseUrl = url || 'http://localhost/';
    // TODO: add security and components to define authentication
    const {
      info, paths, components, security
    } = config;
    this.url = baseUrl;
    this.info = info;
    // Operations
    this.operations = Object.entries(paths).map(
      ([path, pathItem]: Array<any>) => ['get', 'put', 'post', 'delete', 'options', 'head', 'patch', 'trace'].map(
        method => (pathItem[method] && [method, pathItem[method]]) || null
      ).filter(
        entry => entry !== null
      ).map(
        ([method, op]) => new Operation(this.url, path, method, op)
      )
    ).reduce((acc, ops) => acc.concat(ops), []);
    // Security
    const { securitySchemes } = (components || {});
    this.securitySchemes = securitySchemes;
    this.security = security || [];
    // only deal with the first security item
    const securityFirstItem = [...this.security].shift() || {};
    // and the first entry in that item
    const securityFirstEntry = [...Object.entries(securityFirstItem)].shift();
    const [securityKey, securityScopes] = securityFirstEntry || [undefined, []];
    const securitySchemesConfig = securityKey ? securitySchemes[securityKey] : {};
    this.securityConfig = {
      ...securitySchemesConfig,
      scopes: securityScopes,
      ...securityOptions
    };
    // TODO: make this generic
    // if (securityKey) {
    //   switch (securitySchemesConfig.type) {
    //     case 'openIdConnect':
    //       this.securityHandler = new OAuth2Handler(this.securityConfig);
    //       break;
    //     default:
    //       throw new Error(`Unsupported security type '${securitySchemesConfig.type}' for security scheme '${securityKey}'`);
    //   }
    // } else {
    //   this.securityHandler = new NullHandler();
    // }
    // TODO: add the response handler
  }

  /**
   * Returns an operation given its path and method.
   * @param {String} path The path for the operation
   * @param {String} method The method for the operation
   */
  operationByPathAndMethod(path: string, method: string) {
    const uMethod = (method && method.toUpperCase()) || 'GET';
    return this.operations.find(op => op.method === uMethod && op.path === path);
  }

  /**
   * Returns an operation given its ID, as defined in the path item.
   * @param {String} operationId The operation ID.
   */
  operationById(operationId: string) {
    const operation = this.operations.find(op => op.operationId === operationId);
    if(operation) {
      return operation;
    }
    throw new Error(`Operation ${operationId} not found`);
  }

  /**
   * Returns a request for the given operation ID, parameters and options,
   * and wraps security on top.
   * @param {String} operationId The ID of the operation
   * @param {Object} params The parameters to pass to the request
   * @param {Object} options The options to pass to the request
   */
  request(operationId: string, params: any, options: any) {
    const operation = this.operationById(operationId);
    return operation.request(params, options);
  }

  /**
   * Create a thunk that performs response and security handling, given an
   * operation ID and an optional request thunk.
   * @param {String} operationId the operation ID
   * @param {Function} requestThunk the request thunk, defaults to RestRequest.thunk()
   * @returns {Function} a full thunk that includes the security layer
   */
  thunk(operationId: string, requestThunk?: any) {
    const bareRequestThunk = requestThunk || RestRequest.thunk();
    const operation = this.operationById(operationId);
    return this.securityHandler.thunk(
      operation.thunk(bareRequestThunk)
    );
  }

  /**
   * Fetches the response for the given request using the default request
   * thunk. In practice, this just creates the thunk and calls it
   * immediately.
   * @param {RestRequest} request the request to fetch the response for
   * @returns {Promise} a promise that resolves to a RestResponse or
   *   rejects to an ApiError
   */
  fetchResponse(request: RestRequest) {
    const thunk = this.thunk(request.operationId);
    return thunk(request);
  }

  logIn() {
    return this.securityHandler.authenticate();
  }

  logOut() {
    return this.securityHandler.revoke();
  }

  isAuthenticated() {
    return this.securityHandler.isAuthenticated();
  }
}
