import { describe, it } from 'mocha';
import { expect } from 'chai';
// import RestRequest from '../src/RestRequest';
// import ResponseHandler from '../src/ResponseHandler';

describe('ResponseHandler', () => {
  describe('Canary', () => {
    it('should run expect', () => {
      expect(true).to.equal(true)
    });
  });
})

// describe('handle response', () => {
//   it('returns the text response when no responses configured', async () => {
//     // fetchMock.get('http://example.com/helloText', 'The quick brown fox');
//     const handler = new ResponseHandler({});
//     const request = new RestRequest('getHello', 'http://example.com/', 'helloText');
//     const thunk = RestRequest.thunk();
//     const result = await thunk(request).then(
//       response => handler.handleResponse(response, request)
//     );
//     expect(result.request).to.equal(request);
//     expect(result.status).to.equal(200);
//     expect(result.body).to.equal('The quick brown fox');
//   });

//   it('returns the text response with default configured', async () => {
//     // fetchMock.get('http://example.com/helloText', 'The quick brown fox');
//     const handler = new ResponseHandler({
//       default: 'The default response'
//     });
//     const request = new RestRequest('getHello', 'http://example.com/', 'helloText');
//     const thunk = RestRequest.thunk();
//     const result = await thunk(request).then(
//       response => handler.handleResponse(response, request)
//     );
//     expect(result.request).to.equal(request);
//     expect(result.status).to.equal(200);
//     expect(result.body).to.equal('The quick brown fox');
//   });

//   it('returns the text response with status configured', async () => {
//     // fetchMock.get('http://example.com/helloText', 'The quick brown fox');
//     const handler = new ResponseHandler({
//       200: 'The default response'
//     });
//     const request = new RestRequest('getHello', 'http://example.com/', 'helloText');
//     const thunk = RestRequest.thunk();
//     const result = await thunk(request).then(
//       response => handler.handleResponse(response, request)
//     );
//     expect(result.request).to.equal(request);
//     expect(result.status).to.equal(200);
//     expect(result.body).to.equal('The quick brown fox');
//   });

//   it('returns the text response with incorrect status configured', async () => {
//     // fetchMock.get('http://example.com/helloText', 'The quick brown fox');
//     const handler = new ResponseHandler({
//       204: 'The default response'
//     });
//     const request = new RestRequest('getHello', 'http://example.com/', 'helloText');
//     const thunk = RestRequest.thunk();
//     const error = await thunk(request).then(
//       response => handler.handleResponse(response, request)
//     ).catch(e => Promise.resolve(e));
//     expect(error.name).to.equal('StatusError');
//     expect(error.message).to.equal('Expected one of [204], got: 200 - OK');
//   });
// });
