import { describe, it } from 'mocha';
import { expect } from 'chai';
// import RestRequest from '../src/RestRequest';
// import StatusError from '../src/StatusError';
// import ParsingError from '../src/ParsingError';

describe('StatusError', () => {
  describe('Canary', () => {
    it('should run expect', () => {
      expect(true).to.equal(true)
    });
  });
})

// describe('create from request and response', () => {
//   it('unexpected status', async () => {
//     // fetchMock.get('http://example.com/hello202', {
//     //   status: 202,
//     //   body: 'Accepted',
//     //   headers: {
//     //     'Content-Type': 'text/plain'
//     //   }
//     // });
//     const request = new RestRequest('getHello', 'http://example.com/', 'hello202');
//     const thunk = RestRequest.thunk();
//     const result = await thunk(request).then(
//       response => new StatusError('Unexpected status', request, response, ['200'])
//     );
//     expect(result.request).to.equal(request);
//     expect(result.status).to.equal(202);
//     expect(result.expected).to.equal(['200']);
//     expect(result.related).to.equal([]);
//     expect(result.type).to.equal('unexpected');
//     expect(result.isTransient()).to.equal(false);
//   });

//   it('client error', async () => {
//     // fetchMock.get('http://example.com/hello404', {
//     //   status: 404,
//     //   body: 'Not found',
//     //   headers: {
//     //     'Content-Type': 'text/plain'
//     //   }
//     // });
//     const request = new RestRequest('getHello', 'http://example.com/', 'hello404');
//     const thunk = RestRequest.thunk();
//     const result = await thunk(request).then(
//       response => new StatusError('Error status', request, response, ['200'])
//     );
//     expect(result.request).to.equal(request);
//     expect(result.status).to.equal(404);
//     expect(result.expected).to.equal(['200']);
//     expect(result.related).to.equal([]);
//     expect(result.type).to.equal('client');
//     expect(result.isTransient()).to.equal(false);
//   });

//   it('server error', async () => {
//     // fetchMock.get('http://example.com/hello500', {
//     //   status: 500,
//     //   body: 'Internal server error',
//     //   headers: {
//     //     'Content-Type': 'text/plain'
//     //   }
//     // });
//     const request = new RestRequest('getHello', 'http://example.com/', 'hello500');
//     const thunk = RestRequest.thunk();
//     const result = await thunk(request).then(
//       response => new StatusError('Error status', request, response, ['200'])
//     );
//     expect(result.request).to.equal(request);
//     expect(result.status).to.equal(500);
//     expect(result.expected).to.equal(['200']);
//     expect(result.related).to.equal([]);
//     expect(result.type).to.equal('server');
//     expect(result.isTransient()).to.equal(true);
//   });
// });

// describe('with body', () => {
//   it('not found: text body', async () => {
//     // fetchMock.get('http://example.com/hello404json', {
//     //   status: 404,
//     //   body: 'Not found',
//     //   headers: {
//     //     'Content-Type': 'text/plain'
//     //   }
//     // });
//     const request = new RestRequest('getHello', 'http://example.com/', 'hello404json');
//     const thunk = RestRequest.thunk();
//     const resultNoBody = await thunk(request).then(
//       response => new StatusError('Error status', request, response, ['200'])
//     );
//     expect(resultNoBody.body).to.equal(null);
//     const resultWithBody = await resultNoBody.withBody();
//     expect(resultWithBody.body).to.equal('Not found');
//   });

//   it('validation: JSON body', async () => {
//     // fetchMock.get('http://example.com/hello400json', {
//     //   status: 400,
//     //   body: '{"mandatory": ["field1"]}',
//     //   headers: {
//     //     'Content-Type': 'application/json'
//     //   }
//     // });
//     const request = new RestRequest('getHello', 'http://example.com/', 'hello400json');
//     const thunk = RestRequest.thunk();
//     const resultNoBody = await thunk(request).then(
//       response => new StatusError('Error status', request, response, ['200'])
//     );
//     expect(resultNoBody.body).to.equal(null);
//     const resultWithBody = await resultNoBody.withBody();
//     expect(resultWithBody.body).to.equal({
//       mandatory: ['field1']
//     });
//   });

//   it('broken: text body with JSON content type', async () => {
//     // fetchMock.get('http://example.com/hello500json', {
//     //   status: 500,
//     //   body: 'Broken!',
//     //   headers: {
//     //     'Content-Type': 'application/json'
//     //   }
//     // });
//     const request = new RestRequest('getHello', 'http://example.com/', 'hello500json');
//     const thunk = RestRequest.thunk();
//     const resultNoBody = await thunk(request).then(
//       response => new StatusError('Error status', request, response, ['200'])
//     );
//     expect(resultNoBody.body).to.equal(null);
//     const resultWithBody = await resultNoBody.withBody();
//     expect(resultWithBody.body).to.equal('Broken!');
//     expect(resultWithBody.related.length).to.equal(1);
//     expect(resultWithBody.related[0].name).to.equal('ParsingError');
//     expect((resultWithBody.related[0] as ParsingError).contentType).to.equal('application/json');
//   });

//   it('validation: JSON body with upstream status', async () => {
//     // fetchMock.get('http://example.com/hello500upstream', {
//     //   status: 500,
//     //   body: '{"upstream_status": 429}',
//     //   headers: {
//     //     'Content-Type': 'application/json'
//     //   }
//     // });
//     const request = new RestRequest('getHello', 'http://example.com/', 'hello500upstream');
//     const thunk = RestRequest.thunk();
//     const resultNoBody = await thunk(request).then(
//       response => new StatusError('Error status', request, response, ['200'])
//     );
//     expect(resultNoBody.body).to.equal(null);
//     const resultWithBody = await resultNoBody.withBody();
//     expect(resultWithBody.body).to.equal({
//       upstream_status: 429
//     });
//     expect(resultWithBody.status).to.equal(500);
//     expect(resultWithBody.upstreamStatus).to.equal(429);
//   });
// });
