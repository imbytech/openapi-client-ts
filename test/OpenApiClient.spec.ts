import { describe, it } from 'mocha';
import { expect } from 'chai';

describe('OpenApiClient', () => {
  describe('Canary', () => {
    it('should run expect', () => {
      expect(true).to.equal(true)
    });
  });
})

// jest.mock('expo-app-auth', () => ({
//   authAsync: jest.fn((config) => {
//     const { scopes } = config;
//     if (scopes.includes('fail') || scopes.includes('authfail')) {
//       return Promise.reject(new Error('Authentication failed'));
//     }
//     return Promise.resolve({
//       accessToken: 'Auth Access',
//       tokenType: 'Bearer',
//       refreshToken: 'Auth Refresh'
//     });
//   }),
//   revokeAsync: jest.fn(() => Promise.resolve(true)),
//   refreshAsync: jest.fn((config) => {
//     const { scopes } = config;
//     if (scopes.includes('fail') || scopes.includes('refreshfail')) {
//       return Promise.reject(new Error('Refresh failed'));
//     }
//     return Promise.resolve({
//       accessToken: 'Refreshed Access',
//       tokenType: 'Bearer',
//       refreshToken: 'Refreshed Refresh'
//     });
//   })
// }));

// import AsyncStorage from '@react-native-async-storage/async-storage';
// import fetchMock from 'fetch-mock-jest';
// import OpenApiClient from '../../../src/api/OpenApiClient';
// import Operation from '../../../src/api/Operation';
// import RestRequest from '../../../src/api/RestRequest';
// import { API_URL, OAUTH_CLIENT_ID, OAUTH_ISSUER } from '../../../src/environment.json';
// import {
//   OAUTH_SCOPES, openApiConfig, ACCESS_TOKEN, REFRESH_TOKEN
// } from '../../../src/config';
// import ApplicationError from '../../../src/ApplicationError';

// import fluttrApiDocument from '../../../src/api/documents/FluttrApiDocument.json';

// const config = {
//   info: {
//     title: 'Hello API',
//     version: '1.0.0'
//   },
//   paths: {
//     '/hello': {
//       get: {
//         operationId: 'getHello'
//       }
//     },
//     '/greetings': {
//       get: {
//         operationId: 'getGreetings'
//       },
//       post: {
//         operationId: 'postGreetings'
//       }
//     },
//     '/greetings/{id}': {
//       get: {
//         operationId: 'getGreetingsId',
//         parameters: [{
//           name: 'id',
//           in: 'path'
//         }]
//       },
//       put: {
//         operationId: 'putGreetingsId',
//         parameters: [{
//           name: 'id',
//           in: 'path'
//         }]
//       }
//     },
//     '/error': {
//       post: {
//         operationId: 'postError'
//       }
//     }
//   }
// };

// const configWithSecurity = {
//   ...config,
//   components: {
//     securitySchemes: {
//       fluttr: {
//         type: 'openIdConnect',
//         openIdConnectUrl: 'https://fluttr.eu.auth0.com/.well-known/openid-configuration'
//       }
//     }
//   },
//   security: [{
//     fluttr: ['openid', 'email']
//   }]
// };

// describe('create client', () => {
//   it('with no server options', () => {
//     const client = new OpenApiClient(config);
//     expect(client.url).toStrictEqual('http://localhost/');
//     expect(client.operations.length).toBe(6);
//     expect(client.operations[0]).toStrictEqual(new Operation('http://localhost/', '/hello', 'get', config.paths['/hello'].get));
//   });

//   it('with external server object', () => {
//     const options = {
//       servers: [{
//         url: 'http://example.com/'
//       }]
//     };
//     const client = new OpenApiClient({ ...config, ...options });
//     expect(client.url).toStrictEqual('http://example.com/');
//     expect(client.operations.length).toBe(6);
//     expect(client.operations[0]).toStrictEqual(new Operation('http://example.com/', '/hello', 'get', config.paths['/hello'].get));
//   });

//   it('with config server only', () => {
//     const serverConfig = {
//       ...config,
//       servers: [{
//         url: 'http://example.com/'
//       }]
//     };
//     const client = new OpenApiClient(serverConfig);
//     expect(client.url).toStrictEqual('http://example.com/');
//     expect(client.operations.length).toBe(6);
//     expect(client.operations[0]).toStrictEqual(new Operation('http://example.com/', '/hello', 'get', config.paths['/hello'].get));
//   });

//   it('with config server and override', () => {
//     const options = {
//       servers: [{
//         url: 'http://hello.com/'
//       }]
//     };
//     const serverConfig = {
//       ...config,
//       servers: [{
//         url: 'http://example.com/'
//       }]
//     };
//     const client = new OpenApiClient({ ...serverConfig, ...options });
//     expect(client.url).toStrictEqual('http://hello.com/');
//     expect(client.operations.length).toBe(6);
//     expect(client.operations[0]).toStrictEqual(new Operation('http://hello.com/', '/hello', 'get', config.paths['/hello'].get));
//   });

//   it('with server and security', () => {
//     const options = {
//       servers: [{
//         url: 'http://example.com/'
//       }]
//     };
//     const client = new OpenApiClient(
//       { ...configWithSecurity, ...options }
//     );
//     expect(client.url).toStrictEqual('http://example.com/');
//     expect(client.operations.length).toBe(6);
//     expect(client.operations[0]).toStrictEqual(new Operation('http://example.com/', '/hello', 'get', config.paths['/hello'].get));
//     expect(client.securityConfig).toStrictEqual({
//       type: 'openIdConnect',
//       openIdConnectUrl: 'https://fluttr.eu.auth0.com/.well-known/openid-configuration',
//       scopes: ['openid', 'email']
//     });
//   });

//   it('with server, security and security options', () => {
//     const options = {
//       servers: [{
//         url: 'http://example.com/'
//       }]
//     };
//     const securityOptions = {
//       clientId: 'Fluttr Client'
//     };
//     const client = new OpenApiClient(
//       { ...configWithSecurity, ...options }, securityOptions
//     );
//     expect(client.url).toStrictEqual('http://example.com/');
//     expect(client.operations.length).toBe(6);
//     expect(client.operations[0]).toStrictEqual(new Operation('http://example.com/', '/hello', 'get', config.paths['/hello'].get));
//     expect(client.securityConfig).toStrictEqual({
//       type: 'openIdConnect',
//       openIdConnectUrl: 'https://fluttr.eu.auth0.com/.well-known/openid-configuration',
//       scopes: ['openid', 'email'],
//       clientId: 'Fluttr Client'
//     });
//   });

//   it('with full document, mixin and options', () => {
//     const { mixin, options } = openApiConfig();
//     const client = new OpenApiClient(
//       { ...fluttrApiDocument, ...mixin }, options
//     );
//     expect(client.url).toStrictEqual(`${API_URL}/v1/`);
//     expect(client.securityConfig).toStrictEqual({
//       type: 'openIdConnect',
//       openIdConnectUrl: `${OAUTH_ISSUER}.well-known/openid-configuration`,
//       scopes: OAUTH_SCOPES,
//       clientId: OAUTH_CLIENT_ID,
//       additionalParameters: {
//         prompt: 'login'
//       },
//       tokenMode: 'legacy'
//     });
//   });
// });

// describe('get operation', () => {
//   it('finds an operation based on ID', () => {
//     const client = new OpenApiClient(config);
//     expect(client.operationById('getGreetings')).toStrictEqual(new Operation(
//       'http://localhost/',
//       '/greetings',
//       'get',
//       config.paths['/greetings'].get
//     ));
//   });

//   it('finds an operation based on path and method', () => {
//     const client = new OpenApiClient(config);
//     expect(client.operationByPathAndMethod('/greetings/{id}', 'put')).toStrictEqual(new Operation(
//       'http://localhost/',
//       '/greetings/{id}',
//       'put',
//       config.paths['/greetings/{id}'].put
//     ));
//   });

//   it('with full document, mixin and options', () => {
//     const { mixin, options } = openApiConfig();
//     const client = new OpenApiClient(
//       { ...fluttrApiDocument, ...mixin }, options
//     );
//     expect(client.operationById('getProfile')).toStrictEqual(new Operation(
//       `${API_URL}/v1/`,
//       '/profile',
//       'get',
//       {
//         operationId: 'getProfile',
//         responses: {
//           default: {
//             description: 'Default response'
//           }
//         }
//       }
//     ));
//   });
// });

// describe('GET request', () => {
//   it('simple', () => {
//     const options = {
//       servers: [{
//         url: 'http://example.com/'
//       }]
//     };
//     const securityOptions = {
//       clientId: 'Fluttr Client',
//       tokenStore: 'AUTH_TOKENS'
//     };
//     const client = new OpenApiClient(
//       { ...configWithSecurity, ...options }, securityOptions
//     );
//     expect(client.request('getHello', {}, {})).toStrictEqual(
//       new RestRequest(
//         'getHello',
//         'http://example.com/',
//         '/hello',
//         'GET',
//         {
//           headers: {
//             Accept: 'application/json'
//           }
//         }
//       )
//     );
//   });

//   it('with path parameters', () => {
//     const options = {
//       servers: [{
//         url: 'http://example.com/'
//       }]
//     };
//     const securityOptions = {
//       clientId: 'Fluttr Client',
//       tokenStore: 'AUTH_TOKENS'
//     };
//     const client = new OpenApiClient(
//       { ...configWithSecurity, ...options }, securityOptions
//     );
//     expect(
//       client.request('getGreetingsId', { id: 'world' }, {})
//     ).toStrictEqual(
//       new RestRequest(
//         'getGreetingsId',
//         'http://example.com/',
//         '/greetings/world',
//         'GET',
//         {
//           headers: {
//             Accept: 'application/json'
//           }
//         }
//       )
//     );
//   });

//   it('with legacy token mode', () => {
//     const options = {
//       servers: [{
//         url: 'http://example.com/'
//       }]
//     };
//     const securityOptions = {
//       clientId: 'Fluttr Client',
//       tokenMode: 'legacy'
//     };
//     const client = new OpenApiClient(
//       { ...configWithSecurity, ...options }, securityOptions
//     );
//     expect(client.request('getHello', {}, {})).toStrictEqual(
//       new RestRequest(
//         'getHello',
//         'http://example.com/',
//         '/hello',
//         'GET',
//         {
//           headers: {
//             Accept: 'application/json'
//           }
//         }
//       )
//     );
//   });

//   it('with full document, mixin and options, no parameters', () => {
//     const { mixin, options } = openApiConfig();
//     const client = new OpenApiClient(
//       { ...fluttrApiDocument, ...mixin }, options
//     );
//     expect(client.request('getProfile', {}, {})).toStrictEqual(
//       new RestRequest(
//         'getProfile',
//         `${API_URL}/v1/`,
//         '/profile',
//         'GET',
//         {
//           headers: {
//             Accept: 'application/json'
//           }
//         }
//       )
//     );
//   });
// });

// describe('POST request', () => {
//   it('creates a request from an error object', () => {
//     const { mixin, options } = openApiConfig();
//     const client = new OpenApiClient(
//       { ...fluttrApiDocument, ...mixin }, options
//     );
//     const error = new ApplicationError('Caught error', new Error('Something went wrong'), { foo: 'bar' });
//     expect(client.request('postSupportIssue', error, {})).toStrictEqual(
//       new RestRequest(
//         'postSupportIssue',
//         `${API_URL}/v1/`,
//         '/support/issue',
//         'POST',
//         {
//           headers: {
//             Accept: 'application/json',
//             'Content-Type': 'application/json'
//           },
//           body: JSON.stringify({
//             name: 'ApplicationError',
//             _message: 'Caught error',
//             cause: {},
//             causeInfo: {
//               foo: 'bar'
//             },
//             causeMessage: 'Something went wrong'
//           })
//         }
//       )
//     );
//   });
// });

// describe('fetchResponse', () => {
//   it('fetches a 200 text response with stored tokens and returns', async () => {
//     fetchMock.get('http://example.com/hello?expect=plain', (url, opts) => ({
//       status: 200,
//       headers: {
//         'Content-Type': 'text/plain'
//       },
//       body: `Hello, world! (${opts.headers.Authorization})`
//     }));
//     const options = {
//       servers: [{
//         url: 'http://example.com/'
//       }]
//     };
//     const securityOptions = {
//       clientId: 'Fluttr Client',
//       tokenStore: 'AUTH_TOKENS_FR1'
//     };
//     const tokens = {
//       accessToken: 'ACCESS',
//       tokenType: 'Bearer',
//       refreshToken: 'REFRESH'
//     };
//     await AsyncStorage.setItem(securityOptions.tokenStore, JSON.stringify(tokens));
//     const client = new OpenApiClient(
//       { ...configWithSecurity, ...options }, securityOptions
//     );
//     const request = client.request('getHello', { expect: 'plain' }, {});
//     const result = await client.fetchResponse(request);
//     expect(result.status).toBe(200);
//     expect(result.body).toBe('Hello, world! (Bearer ACCESS)');
//     fetchMock.mockReset();
//   });

//   it('fetches a 200 JSON response with stored tokens and returns', async () => {
//     fetchMock.get('http://example.com/hello?expect=json', (url, opts) => ({
//       status: 200,
//       headers: {
//         'Content-Type': 'application/json'
//       },
//       body: JSON.stringify({
//         message: `Hello, world! (${opts.headers.Authorization})`
//       })
//     }));
//     const options = {
//       servers: [{
//         url: 'http://example.com/'
//       }]
//     };
//     const securityOptions = {
//       clientId: 'Fluttr Client',
//       tokenStore: 'AUTH_TOKENS_FR2'
//     };
//     const tokens = {
//       accessToken: 'ACCESS',
//       tokenType: 'Bearer',
//       refreshToken: 'REFRESH'
//     };
//     await AsyncStorage.setItem(securityOptions.tokenStore, JSON.stringify(tokens));
//     const client = new OpenApiClient(
//       { ...configWithSecurity, ...options }, securityOptions
//     );
//     const request = client.request('getHello', { expect: 'json' }, {});
//     const result = await client.fetchResponse(request);
//     expect(result.status).toBe(200);
//     expect(result.body).toStrictEqual({
//       message: 'Hello, world! (Bearer ACCESS)'
//     });
//     fetchMock.mockReset();
//   });

//   it('fetches a 500 text response with stored tokens and aborts', async () => {
//     fetchMock.get('http://example.com/hello?expect=500', (url, opts) => ({
//       status: 500,
//       headers: {
//         'Content-Type': 'text/plain'
//       },
//       body: `Server Error (${opts.headers.Authorization})`
//     }));
//     const options = {
//       servers: [{
//         url: 'http://example.com/'
//       }]
//     };
//     const securityOptions = {
//       clientId: 'Fluttr Client',
//       tokenStore: 'AUTH_TOKENS_FR3'
//     };
//     const tokens = {
//       accessToken: 'ACCESS',
//       tokenType: 'Bearer',
//       refreshToken: 'REFRESH'
//     };
//     await AsyncStorage.setItem(securityOptions.tokenStore, JSON.stringify(tokens));
//     const client = new OpenApiClient(
//       { ...configWithSecurity, ...options }, securityOptions
//     );
//     const request = client.request('getHello', { expect: '500' }, {});
//     const result = await client.fetchResponse(request).catch(e => Promise.resolve(e));
//     expect(result.name).toBe('StatusError');
//     const { response } = result;
//     expect(response.status).toBe(500);
//     expect(response.body).toBe('Server Error (Bearer ACCESS)');
//     fetchMock.mockReset();
//   });

//   it('fetches a 401 response, retries and returns', async () => {
//     fetchMock.get('http://example.com/hello?expect=401', (url, opts) => {
//       if (opts.headers.Authorization === 'Bearer ACCESS') {
//         return {
//           status: 401,
//           headers: {
//             'Content-Type': 'text/plain'
//           },
//           body: 'Unauthorized'
//         };
//       }
//       return {
//         status: 200,
//         headers: {
//           'Content-Type': 'application/json'
//         },
//         body: JSON.stringify({
//           message: `Hello, world! (${opts.headers.Authorization})`
//         })
//       };
//     });
//     const options = {
//       servers: [{
//         url: 'http://example.com/'
//       }]
//     };
//     const securityOptions = {
//       clientId: 'Fluttr Client',
//       tokenStore: 'AUTH_TOKENS_FR4'
//     };
//     const tokens = {
//       accessToken: 'ACCESS',
//       tokenType: 'Bearer',
//       refreshToken: 'REFRESH'
//     };
//     await AsyncStorage.setItem(securityOptions.tokenStore, JSON.stringify(tokens));
//     const client = new OpenApiClient(
//       { ...configWithSecurity, ...options }, securityOptions
//     );
//     const request = client.request('getHello', { expect: '401' }, {});
//     const result = await client.fetchResponse(request);
//     expect(result.status).toBe(200);
//     expect(result.body).toStrictEqual({
//       message: 'Hello, world! (Bearer Refreshed Access)'
//     });
//     fetchMock.mockReset();
//   });

//   it('authenticates, fetches a 200 JSON and returns', async () => {
//     fetchMock.get('http://example.com/hello?expect=json', (url, opts) => ({
//       status: 200,
//       headers: {
//         'Content-Type': 'application/json'
//       },
//       body: JSON.stringify({
//         message: `Hello, world! (${opts.headers.Authorization})`
//       })
//     }));
//     const options = {
//       servers: [{
//         url: 'http://example.com/'
//       }]
//     };
//     const securityOptions = {
//       clientId: 'Fluttr Client',
//       tokenStore: 'AUTH_TOKENS_FR5'
//     };
//     const client = new OpenApiClient(
//       { ...configWithSecurity, ...options }, securityOptions
//     );
//     const request = client.request('getHello', { expect: 'json' }, {});
//     const result = await client.fetchResponse(request);
//     expect(result.status).toBe(200);
//     expect(result.body).toStrictEqual({
//       message: 'Hello, world! (Bearer Auth Access)'
//     });
//     fetchMock.mockReset();
//   });

//   it('fetches a 200 JSON response with legacy stored tokens and returns', async () => {
//     fetchMock.get('http://example.com/hello?expect=jsonlegacy', (url, opts) => ({
//       status: 200,
//       headers: {
//         'Content-Type': 'application/json'
//       },
//       body: JSON.stringify({
//         message: `Hello, world! (${opts.headers.Authorization})`
//       })
//     }));
//     const options = {
//       servers: [{
//         url: 'http://example.com/'
//       }]
//     };
//     const securityOptions = {
//       clientId: 'Fluttr Client',
//       tokenMode: 'legacy'
//     };
//     const tokens = {
//       accessToken: 'LEGACY ACCESS',
//       tokenType: 'Bearer',
//       refreshToken: 'LEGACY REFRESH'
//     };
//     await AsyncStorage.setItem(ACCESS_TOKEN, tokens.accessToken);
//     await AsyncStorage.setItem(REFRESH_TOKEN, tokens.refreshToken);
//     const client = new OpenApiClient(
//       { ...configWithSecurity, ...options }, securityOptions
//     );
//     const request = client.request('getHello', { expect: 'jsonlegacy' }, {});
//     const result = await client.fetchResponse(request);
//     expect(result.status).toBe(200);
//     expect(result.body).toStrictEqual({
//       message: 'Hello, world! (Bearer LEGACY ACCESS)'
//     });
//     fetchMock.mockReset();
//   });
// });
